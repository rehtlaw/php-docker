FROM php:7.0-fpm

#RUN apt update;apt install -y libzip libzip-dev
RUN apt-get update && apt-get install -y libc-client-dev libkrb5-dev libicu-dev libzip-dev libzip4 && rm -r /var/lib/apt/lists/*
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install pdo_mysql imap intl zip

RUN pecl install xdebug zip
RUN docker-php-ext-enable xdebug

COPY conf/php.ini /etc/php/7.1/fpm/conf.d/40-custom.ini
