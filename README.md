# LEMP stack server dockerized

This is based on a somewhat old tutorial on setting up a nginx + php + mysql
server. I changed the mysql server to MariaDB and generally added some things to make
Mautic work (which is the sole reason I even have this).  

## Notes
- the SQL server is accessable via. `mysql:3306`, so don't try to use
  `localhost` as your database domain
